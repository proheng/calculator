import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class GameSpec {
	String gameType;
	int numberOfPlyers;
	int gameTypeScore;
	
	public GameSpec(String gameType, int numberOfPlyers) {
		super();
		this.gameType = gameType;
		this.numberOfPlyers = numberOfPlyers;
	}
	
	public void setGameTypeScore() {
		this.gameTypeScore = this.getGameTypeScore(this.gameType,this.numberOfPlyers);
	}
	
	public int getGameTypeScore(String gameType, int numberOfPlayers){
		int gameTypeScore = 0;
		JsonObject mainObject = Util.readJsonFile("src/gameTypeScheme.json");
		JsonArray gameTypeArray = (JsonArray) mainObject.get(gameType);
		for(int i=0; i<gameTypeArray.size();i++){
			JsonObject eachGameScheme = (JsonObject) gameTypeArray.get(i);
			int number = eachGameScheme.get("numberOfPlayer").getAsInt();
			if(number == numberOfPlayers){
				gameTypeScore = eachGameScheme.get("score").getAsInt();
				break;
			}
		}	
		return gameTypeScore;
	}
	
}
