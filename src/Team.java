import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class Team {
	int teamId;
	ArrayList<Player> teamPlayerList;
	int teamBaseScore;
	int teamPosition;
	int multipleScoreBetForTeam;
	int teamScoreGainForGamePlay;
	String teamResult;
	int teamAvgScore;
	int scoreForPlayerBasedOnTeamScoreGap;
	
	
	public void setTeamInit(int teamId,int teamBaseScore,int teamPosition,int multipleScoreBetForTeam){
		this.teamId = teamId;
		this.teamBaseScore = teamBaseScore;
		this.teamPosition = teamPosition;
		this.multipleScoreBetForTeam = multipleScoreBetForTeam;
	}
	
	public void setTeam(int teamId, ArrayList<Player> teamPlayerList, int teamBaseScore,
			int teamPosition, int multipleScoreBetForTeam,int teamAvgScore,String result){
		this.teamId = teamId;
		this.teamPlayerList = teamPlayerList;
		this.teamBaseScore = teamBaseScore;
		this.teamPosition = teamPosition;
		this.multipleScoreBetForTeam = multipleScoreBetForTeam;
		this.teamAvgScore = teamAvgScore;
		this.teamResult = result;
	}
	
	public void setTeamResult(String result){
		this.teamResult = result;
	}
	
	public void setTeamScoreGainForGamePlay(JsonArray tournamentTeamScheme, int oppositionTeamBaseScore,int tournamentParameter, int gamePlayTypeScore){
		this.teamScoreGainForGamePlay = finalScoreGain(tournamentTeamScheme,oppositionTeamBaseScore, tournamentParameter,gamePlayTypeScore);
		System.out.println("each team gamePlay gain: " + this.teamScoreGainForGamePlay);
	}
	
	public void setTeamBaseScore(){
		this.teamBaseScore = this.teamBaseScore + this.teamScoreGainForGamePlay;
	}
	
	public void setScoreForPlayerBasedOnTeamScoreGap(int oppositionTeamAveScore){
		int ScoreForPlayerBasedOnTeamScoreGap = 0;
		if((oppositionTeamAveScore - this.teamAvgScore)>130 && (this.teamResult.equals("win"))){
			int avgGap = (int) (oppositionTeamAveScore - this.teamAvgScore) +1;
			ScoreForPlayerBasedOnTeamScoreGap = (int) ((avgGap - 130)/20)*3;
		}else if((this.teamAvgScore - oppositionTeamAveScore)>130 && (this.teamResult.equals("lose"))){
			System.out.println("this team avg score" + this.teamAvgScore);
			int avgGap = this.teamAvgScore -oppositionTeamAveScore;
			ScoreForPlayerBasedOnTeamScoreGap = - (int) ((avgGap - 130)/20)*3;
		}
		this.scoreForPlayerBasedOnTeamScoreGap = ScoreForPlayerBasedOnTeamScoreGap;
	}
	
	public ArrayList<Player> getPlayerList(){
		return this.teamPlayerList;
	}
	
	public int finalScoreGain(JsonArray tournamentTeamScheme, int oppositionTeamBaseScore, int tournamentParameter, int gamePlayTypeScore){
		// get win/draw/lose actual score scheme
		JsonObject teamScoreGainScheme = Util.getScoreGainScheme(this.teamBaseScore, tournamentTeamScheme);
		
		// based on scheme
		int scoreBasedOnScheme = teamScoreGainScheme.get(teamResult).getAsInt();
		
		//based on multipleScoreBetForTeam
		int scoreAddOnmultiple = this.multipleScoreBetForTeam * scoreBasedOnScheme;
		
		// based on opposition team score gap
		int gapScore =Math.abs(this.teamBaseScore - oppositionTeamBaseScore);
		int bounsScore = 0; 
		if(gapScore > 600){
			bounsScore = checkOptionalScoreGain(this.teamBaseScore, oppositionTeamBaseScore, this.teamResult);	
		}
		return scoreAddOnmultiple + bounsScore + tournamentParameter + gamePlayTypeScore;
	}
	
	public int checkOptionalScoreGain(int governmentTeamBaseScore, int oppositionTeamBaseScore, String result){
		int bounsScore = 0;
	
		if((governmentTeamBaseScore< oppositionTeamBaseScore) && (result.equals("win"))){
			int gapScore = (oppositionTeamBaseScore - governmentTeamBaseScore)-600;			
			int bonusScoreGain = (gapScore/ 100) *5;
			bounsScore = bonusScoreGain;		
		}else if((governmentTeamBaseScore > oppositionTeamBaseScore) && (result.equals("lose"))){
			int gapScore = governmentTeamBaseScore-oppositionTeamBaseScore-600;
			int penatyScoreGain = - (gapScore / 100)*5;
			bounsScore = penatyScoreGain;
		}
		return bounsScore;
	}
	
	public int getTeamPlayerAvgScore(ArrayList<Player> playerList){
		int totalScore = 0;
		for(Player p: playerList){
			totalScore+=p.baseScore;
		}
		return totalScore/playerList.size();
	}
	
}
