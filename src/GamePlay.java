


public class GamePlay {
	Tournament tournament;
	GameSpec gameSpec;
	Team governmentTeam;
	Team oppositionTeam;
	
	public GamePlay(Tournament tournament, GameSpec gameSpec) {
		super();
		this.tournament = tournament;
		this.gameSpec = gameSpec;
	}
	
	public void setGamePlayTeam(Team governmentTeam, Team oppositionTeam){
		this.governmentTeam = governmentTeam;
		this.oppositionTeam = oppositionTeam;
	}
	
	public void setGamePlayForTeamWithoutVerseTeam(Team governmentTeam){
		this.governmentTeam = governmentTeam;
	}
	
	public Team getGamePlayGovernmentTeam(){
		return this.governmentTeam;
	}
}
