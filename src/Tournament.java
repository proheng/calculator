import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Tournament {
	int tournamentId;
	int totalPlayerNumber;
	int totalTeamNumber;
	JsonArray tournamentIndividualScheme;
	JsonArray tournamentTeamScheme;

	public Tournament(int tournanmentId, int totalPlayerNumber, int totalTeamNumber) {
		super();
		this.tournamentId = tournanmentId;
		this.totalPlayerNumber = totalPlayerNumber;
		this.totalTeamNumber = totalTeamNumber;
	}
	
	public void setTournamentIndividualScheme(){
		this.tournamentIndividualScheme = this.getTournamentScheme(this.totalPlayerNumber, "individual");
	}
	
	public void setTournamentTeamScheme(){
		this.tournamentTeamScheme = this.getTournamentScheme(this.totalTeamNumber, "team");
	}
	
	public JsonArray getTournamentScheme(int totalNumber, String scoreGainType){
		JsonObject mainObject = Util.readJsonFile("src/calculatorScheme.json");
		JsonArray individualArray = (JsonArray) mainObject.get(scoreGainType);
		JsonArray scoreSchemeBasedOnTournament = new JsonArray();
		
		for(int i=0;i<=individualArray.size();i++){
			JsonObject eachTournamentObject = (JsonObject) individualArray.get(i);
			
			if(checkTournamentScheme(eachTournamentObject,totalNumber)){
				scoreSchemeBasedOnTournament = (JsonArray) eachTournamentObject.get("scoreScheme");
				break;
			};
		}
		return scoreSchemeBasedOnTournament;
	}
	
	public static Boolean checkTournamentScheme(JsonObject TournamentObject, int totalNumber){
		int min = TournamentObject.get("minNumberOfTournamentPlayer").getAsInt();
		int max = TournamentObject.get("maxNumberOfTournamentPlayer").getAsInt();
		if((totalNumber <= max) && (totalNumber >= min)){
			return true;
		}else{
			return false;
		}
	}
}
